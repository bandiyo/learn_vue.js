// console.log('Hello Vue')

const app = Vue.createApp({
	// template: '<h2> I am the template </h2>'
	data(){
		return {
			showBooks : true,
			url : 'http://www.thenetninja.co.uk',
			books : [
				{title : 'name of the wind', author : 'patrick rothfuss', img : 'assets/1.png',isFav : true},
				{title : 'the way of kings', author : 'brandon sanderson', img : 'assets/2.png',isFav : false},
				{title : 'the final empire', author : 'brandon sanderson', img : 'assets/3.png',isFav : true},
			]
		}
	},

	methods : {
		// changeTitle(title){
		// 	// console.log('you clicked me')
		// 	this.title = title
		// }
		toggleShowBooks() {
			this.showBooks = !this.showBooks
		},
		handleEvent(e, data) {
			console.log(e, e.type)
			if (data) {
				console.log(data)
			}
		},
		handleMousemove(e) {
			this.x = e.offsetX
			this.y = e.offsetY
		},

		toggleFav(book) {
			book.isFav = !book.isFav
		}
	},

	computed : {
		filteredBooks(){
			return this.books.filter((book) => book.isFav)
		}
	}

})

app.mount('#app')